import { Component, ReactNode, PropsWithChildren } from 'react';

class ErrorBoundary extends Component<PropsWithChildren<unknown>> {
  componentDidCatch(): void {
    window.location.href = '/error';
  }

  render(): ReactNode {
    return this.props.children;
  }
}

export default ErrorBoundary;