import React, { ReactElement} from 'react';
import { Outlet } from 'react-router-dom';
import './MainLayout.styles.sass';

const MainLayout = (): ReactElement => {
  return (
    <div className='main-layout__root'>
      <Outlet />
    </div>
  )
};

export default MainLayout;
