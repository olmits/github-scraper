import React, { ReactElement } from 'react';
import { PrivateUser, PublicUser } from '../../api';
import './ProfileImage.styles.sass';

const ProfileImage = ({ avatar_url: avatarUrl }: Partial<Pick<PrivateUser | PublicUser, 'avatar_url'>>): ReactElement => {
  return (
    <div className='ProfileImage__root'>
      <img className='ProfileImage__image' width={100} height={100} src={avatarUrl} />
    </div>
  )
}

export default ProfileImage;
