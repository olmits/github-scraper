import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import ProfileImage from './ProfileImage';

describe('<ProfileImage /> testing', () => {
  let component: RenderResult;

  afterEach(() => {
    jest.clearAllMocks();
  });

  beforeEach(() => {
    component = render(
      <ProfileImage avatar_url='/test' />
    );
  });

  describe('check <ProfileImage /> DOM', () => {
    it('should render <ProfileImage />', () => {
      expect(component.baseElement).toMatchSnapshot();
    });
  });
});

