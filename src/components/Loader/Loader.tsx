import React, { ReactElement, PropsWithChildren } from 'react';
import classNames from 'classnames';
import './Loader.styles.sass';

interface LoaderProps {
  isLoading: boolean;
  dataTestId?: string; 
}

const Loader = ({ isLoading, dataTestId, children }: PropsWithChildren<LoaderProps>): ReactElement => {
  return (
    <div
      className={classNames('Loader__root', { 'Loader__root--loading': isLoading })} 
      data-testid={dataTestId || 'Loader__root'}
      role='progressbar'
      aria-live='polite'
      aria-busy={isLoading}
    >
      {children}
      {isLoading && (
        <div className='Loader__wrapper'>
          <div className='Loader__progress'>
            <span />
            <span />
            <span />
          </div>
        </div>
      )}
    </div>
  )
};

export default Loader;
