import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import Loader from './Loader';

describe('<Loader /> testing', () => {
  let component: RenderResult;

  afterEach(() => {
    jest.clearAllMocks();
  });

  beforeEach(() => {
    component = render(
      <Loader isLoading>TEST</Loader>
    );
  });

  describe('check <Loader /> DOM', () => {
    it('should render <Button />', () => {
      expect(component.baseElement).toMatchSnapshot();
    });
  });
});

