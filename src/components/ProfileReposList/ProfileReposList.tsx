import React, { ReactElement } from 'react';
import { PublicRepository } from '../../api';
import Pagination, { PaginationProps } from '../Pagination';
import './ProfileReposList.styles.sass';

interface ProfileReposListProps extends PaginationProps {
  data?: PublicRepository[];
}

const ProfileReposList = ({ data, ...rest }: ProfileReposListProps): ReactElement => {
  return (
    <div className='ProfileReposList__root'>
      <ul className='ProfileReposList__list'>
        <li className='ProfileReposList__list-item'>
          <div className='ProfileReposList__list-item__repo-name'>Name</div>
          <div className='ProfileReposList__list-item__repo-description'>Description</div>
        </li>
        {data?.map(({ id, name, description }) => (
          <li key={id} className='ProfileReposList__list-item'>
            <div className='ProfileReposList__list-item__repo-name'>{name}</div>
            <div className='ProfileReposList__list-item__repo-description'>{description as string || 'N/A'}</div>
          </li>
        ))}
      </ul>
      <Pagination {...rest} />
    </div>
  )
}

export default ProfileReposList;
