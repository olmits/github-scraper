import React, { ReactElement, useCallback, useMemo } from 'react';
import Button from '../Button';
import './Pagination.styles.sass';

export interface PaginationProps {
  page: number;
  pageSize: number;
  count: number;
  onChangePage: (newPage: number) => void;
}

const Pagination = ({
  page,
  pageSize,
  count,
  onChangePage,
}: PaginationProps): ReactElement => {
  const pages = useMemo(() => Math.ceil(count / pageSize), [count, pageSize]);

  const handleClickPrev = useCallback(() => {
    onChangePage(page - 1);
  }, [page, onChangePage]);
  
  const handleClickNext = useCallback(() => {
    onChangePage(page + 1);
  }, [page, onChangePage]);

  return (
    <div className='Pagination__root'>
      <div className='Pagination__button-prev'>
        <Button
          onClick={handleClickPrev}
          disabled={page <= 1}
          dataTestId='pagination-prev-button'
        >
          PREV
        </Button>
      </div>
      <div className='Pagination__page-number'>
        {page} / {pages}
      </div>
      <div className='Pagination__button-next'>
        <Button
          onClick={handleClickNext}
          disabled={!pages || page === pages}
          dataTestId='pagination-next-button'
        >
          NEXT
        </Button>
      </div>
    </div>
  )
}

export default Pagination;
