import React from 'react';
import { render, fireEvent, RenderResult } from '@testing-library/react';
import Pagination from './Pagination';

describe('<Pagination /> testing', () => {
  let component: RenderResult;
  const onChangePageMock = jest.fn();

  afterEach(() => {
    jest.clearAllMocks();
  });

  beforeEach(() => {
    component = render(
      <Pagination
        page={1}
        pageSize={5}
        count={10}
        onChangePage={onChangePageMock}
      />
    );
  });

  describe('check <Pagination /> DOM', () => {
    it('should render <Pagination />', () => {
      expect(component.baseElement).toMatchSnapshot();
    });
  });

  describe('check <Pagination /> actions', () => {
    it('should trigger onChangePage', () => {
      fireEvent.click(component.getByTestId('pagination-next-button'));

      expect(onChangePageMock).toHaveBeenCalled();
      expect(onChangePageMock).toHaveBeenCalledTimes(1);
      expect(onChangePageMock).toHaveBeenCalledWith(2);
    });
  });
});

