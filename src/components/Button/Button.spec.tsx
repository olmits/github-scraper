import React from 'react';
import { render, screen, fireEvent, RenderResult } from '@testing-library/react';
import Button from './Button';

describe('<Button /> testing', () => {
  const onClickMock = jest.fn();
  let component: RenderResult;

  afterEach(() => {
    jest.clearAllMocks();
  });

  beforeEach(() => {
    component = render(
      <Button onClick={onClickMock}>TEST</Button>
    );
  });

  describe('check <Button /> DOM', () => {
    it('should render <Button />', () => {
      expect(component.baseElement).toMatchSnapshot();
    });
    it('should render disabled <Button />', () => {
      const { rerender } = component;

      rerender(<Button onClick={onClickMock} disabled>TEST</Button>)
      
      expect(component.baseElement).toMatchSnapshot();
    });
  });

  describe('check <Button /> events', () => {
    it('should trigger onClick event', () => {
      fireEvent.click(screen.getByTestId('button'));

      expect(onClickMock).toHaveBeenCalled();
      expect(onClickMock).toHaveBeenCalledTimes(1);
    });
  });
});

