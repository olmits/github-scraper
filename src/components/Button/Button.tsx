import React, { memo, ReactElement, PropsWithChildren, MouseEventHandler } from 'react';
import classNames from 'classnames';
import './Button.styles.sass';

interface ButtonProps {
  onClick: MouseEventHandler<HTMLButtonElement>;
  disabled?: boolean;
  dataTestId?: string; 
}

const Button = ({
  children,
  onClick,
  disabled,
  dataTestId,
}: PropsWithChildren<ButtonProps>): ReactElement => {
  return (
    <button
      className={classNames('Button__root', { 'Button__root--disabled': disabled })}
      data-testid={dataTestId || 'button'}
      onClick={onClick}
    >
      {children}
    </button>
  )
};

export default memo(Button);
