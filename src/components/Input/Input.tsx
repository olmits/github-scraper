import React, { ChangeEventHandler, ReactElement, useState, useCallback } from 'react';
import classNames from 'classnames';
import './Input.styles.sass';

interface InputProps {
  id: string;
  label: string;
  value: string;
  onChange: ChangeEventHandler<HTMLInputElement>;
}

const Input = ({
  id,
  label,
  value,
  onChange,
}: InputProps): ReactElement => {
  const [focus, setFocus] = useState(false);

  const handleFocus = useCallback(() => {
    setFocus(true);
  }, []);

  const handleBlur = useCallback(() => {
    setFocus(false);
  }, []);

  return (
    <div className='Input__root'>
      <label
        className={classNames('Input__label', { 'Input__label--focus': focus, 'Input__label--filled': value.length > 0 })}
        data-testid='Input__label'
        htmlFor={id}
      >
        {label}
      </label>
      <div
        className={classNames('Input__input-container', { 'Input__input-container--focus': focus })}
        data-testid='Input__input-container'
      >
        <input
          id={id}
          className='Input__input'
          data-testid='Input__input'
          onChange={onChange}
          onBlur={handleBlur}
          onFocus={handleFocus}
          value={value}
        />
      </div>
    </div>
  )
};

export default Input;
