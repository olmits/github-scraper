import React from 'react';
import { render, screen, fireEvent, RenderResult } from '@testing-library/react';
import Input from './Input';

describe('<Input /> testing', () => {
  const onChangeMock = jest.fn();
  let component: RenderResult;

  afterEach(() => {
    jest.clearAllMocks();
  });

  beforeEach(() => {
    component = render(
      <Input
        id='id'
        label='label'
        value=''
        onChange={onChangeMock}
      />
    );
  });

  describe('check <Input /> DOM', () => {
    it('should render <Input />', () => {
      expect(component.baseElement).toMatchSnapshot();
    });
    it('should check focus state', async () => {
      fireEvent.focus(screen.getByTestId('Input__input'));
      
      expect(component.baseElement).toMatchSnapshot();
    });
    it('should check filled data props', async () => {
      const { rerender } = component;
      rerender(
        <Input
          id='id'
          label='label'
          value='some value'
          onChange={onChangeMock}
        />
      );
      
      expect(component.baseElement).toMatchSnapshot();
    });

  });

  describe('check <Input /> events', () => {
    it('should trigger onChange event', () => {
      fireEvent.change(screen.getByTestId('Input__input'), { target: { value: 'test value' }});

      expect(onChangeMock).toHaveBeenCalled();
      expect(onChangeMock).toHaveBeenCalledTimes(1);
    });
  });
});

