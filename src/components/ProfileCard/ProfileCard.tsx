import React, { ReactElement } from 'react';
import { PrivateUser, PublicUser } from '../../api';
import ProfileImage from '../ProfileImage';
import './ProfileCard.styles.sass';

interface ProfileCardProps {
  data?: PrivateUser | PublicUser
}

const ProfileCard = ({ data }: ProfileCardProps): ReactElement => {
  return (
    <div className='ProfileCard__root'>
      <ProfileImage avatar_url={data?.avatar_url} />
      <div className='ProfileCard__content'>
        <div className='ProfileCard__content__name' data-testid='ProfileCard__content__name'>
          Name: {data?.name || 'N/A'}
        </div>
        <div className='ProfileCard__content__repos-count' data-testid='ProfileCard__content__repos-count'>
          Total number of repositories: {data?.public_repos}
        </div>
      </div>
    </div>
  )
}

export default ProfileCard;
