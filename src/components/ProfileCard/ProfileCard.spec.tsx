import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import { getGetUserByUsernameMock } from '../../api/__generated__/user/user.msw';
import ProfileCard from './ProfileCard';

describe('<ProfileCard /> testing', () => {
  let component: RenderResult;
  const data = getGetUserByUsernameMock();

  afterEach(() => {
    jest.clearAllMocks();
  });

  beforeEach(() => {
    component = render(
      <ProfileCard data={data} />
    );
  });

  describe('check <ProfileCard /> DOM', () => {
    it('should render <ProfileCard />', () => {
      expect(component.baseElement).toMatchSnapshot();
    });
  });
});

