import React from 'react';
import Router, { MemoryRouter } from 'react-router-dom';
import { QueryClientProvider } from 'react-query';
import { render, screen, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom'
import queryClient from '../../api/queryClient';
import User from './User';

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useParams: jest.fn(),
 }));

describe('<User /> testing', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('check <User /> DOM', () => {
    it('should render <User /> name', async () => {
      jest.spyOn(Router, 'useParams').mockReturnValue({ username: 'test' });

      render(
        <MemoryRouter>
          <QueryClientProvider client={queryClient}>
            <User />
          </QueryClientProvider>
        </MemoryRouter>
      );

      await waitFor(() => {
        expect(screen.getByTestId('user-loading')).toHaveAttribute('aria-busy', 'true');
      });
      await waitFor(() => {
        expect(screen.getByTestId('user-loading')).toHaveAttribute('aria-busy', 'false');
      }, { timeout: 3000 });
      
      expect(screen.getByTestId('ProfileCard__content__name').textContent)
        .toEqual('Name: Test name')
      expect(screen.getByTestId('ProfileCard__content__repos-count').textContent)
        .toEqual('Total number of repositories: 12')
    });
  });
});

