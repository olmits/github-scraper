import React, { ReactElement, useCallback, useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { useGetUserByUsername, useGetReposByUsername } from '../../api/__generated__';
import Button from '../../components/Button';
import Loader from '../../components/Loader';
import ProfileCard from '../../components/ProfileCard';
import ProfileReposList from '../../components/ProfileReposList';
import './User.styles.sass'

const PAGE_SIZE: number = 5;

const User = (): ReactElement => {
  const [page, setPage] = useState(1);

  const { username } = useParams();
  const navigate = useNavigate();

  const { data: user, isLoading: isUserLoading } = useGetUserByUsername(
    username,
    { query: { enabled: !!username }}
  );

  const { data: repos, isLoading: isReposLoading } = useGetReposByUsername(
    username,
    { page, per_page: PAGE_SIZE },
    { query: { enabled: !!user } }
  );
  
  const count = user?.public_repos || 0;

  const handleChangePage = useCallback((newPage: number) => {
    setPage(newPage);
  }, []);

  const handleBack = useCallback(() => {
    navigate('/');
  }, []);

  return (
    <div className='User__root'>
      <Button onClick={handleBack}>BACK</Button>
      <Loader isLoading={isUserLoading} dataTestId='user-loading'>
        <ProfileCard data={user} />
      </Loader>
      <Loader isLoading={isReposLoading} dataTestId='repos-loading'>
        <ProfileReposList
          data={repos}
          page={page}
          pageSize={PAGE_SIZE}
          count={count}
          onChangePage={handleChangePage}
        />
      </Loader>
    </div>
  )
}

export default User;