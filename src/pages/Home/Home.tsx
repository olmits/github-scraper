import React, { useState, ReactElement, ChangeEventHandler } from 'react';
import { useNavigate } from 'react-router-dom';
import Button from '../../components/Button';
import Input from '../../components/Input';
import './Home.styles.sass'

const Home = (): ReactElement => {
  const navigate = useNavigate();

  const [value, setValue] = useState('');

  const handleChangeValue: ChangeEventHandler<HTMLInputElement> = (e) => {
    setValue(e.target.value);
  }

  const handleClick = () => {
    if (value) {
      navigate(`user/${value}`);
    }
  }

  return (
    <div className='Home__root'>
      <Input
        id='search-input'
        label='Search'
        value={value}
        onChange={handleChangeValue}
      />
      <Button
        onClick={handleClick}
        disabled={!value.length}
        dataTestId='search-user-button'
      >
        Search
      </Button>
    </div>
  )
}

export default Home;