import React from 'react';
import Router, { MemoryRouter } from 'react-router-dom';
import { fireEvent, render, RenderResult } from '@testing-library/react';
import Home from './Home';

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useNavigate: jest.fn(),
 }));

describe('<Home /> testing', () => {
  let component: RenderResult;
  const navigate = jest.fn()

  afterEach(() => {
    jest.clearAllMocks();
  });

  beforeEach(() => {
    jest.spyOn(Router, 'useNavigate').mockImplementation(() => navigate);
    
    component = render(
      <MemoryRouter>
        <Home />
      </MemoryRouter>
    );
  });

  describe('check <Home /> DOM', () => {
    it('should render <Home />', () => {
      expect(component.baseElement).toMatchSnapshot();
    });
  });

  describe('check <Home /> actions', () => {
    it('should return home', () => {
      fireEvent.change(component.getByTestId('Input__input'), { target: { value: 'test' }});
      fireEvent.click(component.getByTestId('search-user-button'));

      expect(navigate).toHaveBeenCalled();
      expect(navigate).toHaveBeenCalledTimes(1);
      expect(navigate).toHaveBeenCalledWith('user/test');
    });
  });
});

