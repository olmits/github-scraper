import React from 'react';
import Router, { MemoryRouter } from 'react-router-dom';
import { fireEvent, render, RenderResult } from '@testing-library/react';
import NotFound from './NotFound';

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useNavigate: jest.fn(),
 }));

describe('<NotFound /> testing', () => {
  let component: RenderResult;
  const navigate = jest.fn()

  afterEach(() => {
    jest.clearAllMocks();
  });

  beforeEach(() => {
    jest.spyOn(Router, 'useNavigate').mockImplementation(() => navigate);
    
    component = render(
      <MemoryRouter>
        <NotFound />
      </MemoryRouter>
    );
  });

  describe('check <NotFound /> DOM', () => {
    it('should render <NotFound />', () => {
      expect(component.baseElement).toMatchSnapshot();
    });
  });

  describe('check <NotFound /> actions', () => {
    it('should return home', () => {
      fireEvent.click(component.getByTestId('navigate-home-button'));

      expect(navigate).toHaveBeenCalled();
      expect(navigate).toHaveBeenCalledTimes(1);
    });
  });
});

