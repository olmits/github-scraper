import React, { useCallback, ReactElement } from 'react';
import { useNavigate } from 'react-router-dom';
import Button from '../../components/Button';
import './NotFound.styles.sass';

const NotFound = (): ReactElement => {
  const navigate = useNavigate();

  const handleNavigateHome = useCallback(() => {
    navigate('/');
  }, []);

  return (
    <div className='NotFound__root'>
      <Button onClick={handleNavigateHome} dataTestId='navigate-home-button'>HOME</Button>
      <div className='NotFound__message'>
        404
      </div>
    </div>
  )
}

export default NotFound;