import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.sass';
import App from './App';

if (process.env.NODE_ENV === 'test') {
  const { server } = require('./mocks/server');
  server.start()
}

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
