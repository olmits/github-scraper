# Github scraper app

## Description

Small project to search Github user info and public repositories.

Main tech stack:

- React
- React Query
- Axios
- Typescript
- SASS

`jest` and `@testing-library` are used for test.

`orval` and `msw` to build and mock api requests for `react-query`.

## Setup

Ensure the current **node** version 16.x.x or above.

Install the packages:

```bash
npm i
```

Run the dev server:

```bash
npm start
```

## File structure

- `/api` - dir for `react-query` generated custom hooks.
- `/layouts` - reusable layouts for `react-router-dom`.
- `/component` - common reusable components directory.
- `/mocks` - dir for setup `msw` mocks.
- `/pages` - app pages.
- `/utils` - should contain app developer-kit (helpers, configs, hooks), but there's only ErrorBoundary
