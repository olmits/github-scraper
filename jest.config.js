
const config = {
  verbose: true,
  testEnvironment: 'jsdom',
  moduleFileExtensions: ['js', 'jsx', 'ts', 'tsx'],
  moduleNameMapper: {
    "^.+\\.(css|sass|scss)$": "babel-jest"
  },
  transform: {
    "^.+\\.jsx?$": "babel-jest",
    "^.+\\.tsx?$": "ts-jest"
  },
  collectCoverageFrom: [
    "src/**/*.{js, jsx, ts, tsx}",
    "!src/*.{ts, tsx}",
    "!src/api/",
    "!src/mocks/",
  ]
};

module.exports = config;