module.exports = {
  scraper: {
    input: {
      target: 'src/api/api.yaml'
    },
    output: {
      mode: 'tags-split',
      target: './scraper.ts',
      schemas: './schemas',
      client: 'react-query',
      workspace: 'src/api/__generated__',
      prettier: true,
      mock: true,
      override: {
        mutator: {
          path: '../api.ts',
          name: 'customInstance'
        },
        operations: {
          getUserByUsername: {
            mock: {
              properties: () => ({
                name: 'Test name',
                avatar_url: 'test',
                public_repos: 12
              }),
            }
          }
        }
      }
    }
  }
}
